-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `smart_attendance`;
CREATE DATABASE `smart_attendance` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smart_attendance`;

DROP TABLE IF EXISTS `aaa_auth`;
CREATE TABLE `aaa_auth` (
  `handle` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caption` varchar(48) NOT NULL DEFAULT '' COMMENT 'app name',
  `ip` varchar(16) NOT NULL DEFAULT '' COMMENT 'auth filter ip',
  `key` varchar(40) NOT NULL DEFAULT '' COMMENT 'auth secret key',
  `description` text NOT NULL,
  PRIMARY KEY (`handle`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `aaa_auth`;
INSERT INTO `aaa_auth` (`handle`, `caption`, `ip`, `key`, `description`) VALUES
(1,	'Client Test',	'::1',	'HEY6P2AHTG4FH1ZVYG97QLS7DN4P6BRQ2491MRRM',	'Default Client Test');

DROP TABLE IF EXISTS `aaa_token`;
CREATE TABLE `aaa_token` (
  `auth` int(10) unsigned NOT NULL,
  `account` int(10) unsigned DEFAULT NULL,
  `value` varchar(40) NOT NULL DEFAULT '' COMMENT 'token value',
  `time` datetime NOT NULL COMMENT 'CronDaily: Delete if time < NOW()',
  UNIQUE KEY `value` (`value`),
  UNIQUE KEY `auth_account` (`auth`,`account`),
  KEY `auth` (`auth`),
  KEY `account` (`account`),
  CONSTRAINT `aaa_token_ibfk_1` FOREIGN KEY (`auth`) REFERENCES `aaa_auth` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `aaa_token_ibfk_2` FOREIGN KEY (`account`) REFERENCES `account` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `aaa_token`;
INSERT INTO `aaa_token` (`auth`, `account`, `value`, `time`) VALUES
(1,	NULL,	'0Y3QA38YD9E64X41F0V7SEA09U4IYY2U88DPXZJR',	'2019-05-13 13:29:49'),
(1,	4,	'8P5KDB8E995TQQPMV6J1MSIGUSKTTJUTIMK4TNHJ',	'2019-05-14 03:45:12'),
(1,	NULL,	'CTO9H5I2NX8C6ZR2XL4MW7RKYRA85C65ZYQQ928P',	'2019-05-13 13:29:54'),
(1,	NULL,	'D8EPA1I60ZS0DISDS6WAEIURSKXJJEVM80WZ0BI5',	'2019-05-13 13:29:48'),
(1,	NULL,	'EHDFF8KAAK9XMNDL05D70C0F8JS3MBSW9DPUC7GY',	'2019-05-13 16:32:51'),
(1,	NULL,	'IYB9XOS72R5P915LG0JWB0M839H1HFJAGZMQYTOA',	'2019-05-13 15:47:29'),
(1,	NULL,	'JFGX8WZAPE7L63CROY1LB4RZNKMZ8961PR5H2OF2',	'2019-05-13 16:13:16'),
(1,	1,	'N558UIG5RLSJWHZ9PSIXORE2NA6NV6CBQ5MB8JB6',	'2019-05-14 03:08:41');

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `handle` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(24) NOT NULL DEFAULT '',
  `password` varchar(28) NOT NULL DEFAULT '' COMMENT 'base64(sha1())',
  `nickname` varchar(24) NOT NULL DEFAULT '',
  `privilege` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0x01 - System Administrator; 0x02 - HRD Day off; 0x04 - HRD Event (TU); 0x08 - Supervisior',
  PRIMARY KEY (`handle`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `account`;
INSERT INTO `account` (`handle`, `username`, `password`, `nickname`, `privilege`) VALUES
(1,	'papatong',	'oYR+rsv40s8Iqzxa8wreycUFGJ8=',	'samy',	65535),
(2,	'staff',	'bMtLfDmm53927PqTWoVcbEatVhE=',	'staff',	0),
(3,	'supervisior',	'/WHTZ9nTNYTWCXLJN6vDxv/HJSk=',	'supervisior',	0),
(4,	'hrd',	'/yyT/cCIEMYYnpCGkFqPvBrRFAQ=',	'hrd',	2);

DROP TABLE IF EXISTS `account_hierarchy`;
CREATE TABLE `account_hierarchy` (
  `account` int(10) unsigned NOT NULL,
  `supervisior` int(10) unsigned NOT NULL,
  UNIQUE KEY `account_u` (`account`),
  KEY `account_i` (`account`),
  KEY `supervisior` (`supervisior`),
  CONSTRAINT `account_hierarchy_ibfk_3` FOREIGN KEY (`account`) REFERENCES `account` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `account_hierarchy_ibfk_4` FOREIGN KEY (`supervisior`) REFERENCES `account` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `account_hierarchy`;
INSERT INTO `account_hierarchy` (`account`, `supervisior`) VALUES
(1,	1),
(2,	3);

DROP TABLE IF EXISTS `account_profile`;
CREATE TABLE `account_profile` (
  `account` int(10) unsigned NOT NULL,
  `id` varchar(24) NOT NULL DEFAULT '' COMMENT 'NIP',
  `fullname` varchar(48) NOT NULL DEFAULT '',
  `unit` varchar(128) NOT NULL DEFAULT '' COMMENT 'organizational unit',
  `address` varchar(512) NOT NULL DEFAULT '',
  `phone` varchar(24) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  KEY `account` (`account`),
  CONSTRAINT `account_profile_ibfk_1` FOREIGN KEY (`account`) REFERENCES `account` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `account_profile`;

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance` (
  `handle` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account` int(10) unsigned NOT NULL,
  `onduty` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `offduty` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`handle`),
  KEY `account` (`account`),
  CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`account`) REFERENCES `account` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `attendance`;
INSERT INTO `attendance` (`handle`, `account`, `onduty`, `offduty`, `time`) VALUES
(1,	2,	'2018-04-29 11:20:00',	'2019-04-30 11:20:00',	'2019-05-13 03:45:12'),
(5,	2,	'2019-04-30 07:00:00',	'1970-01-01 00:00:00',	'2019-04-30 14:38:25'),
(10,	2,	'2019-04-30 07:00:00',	'2019-04-30 16:20:00',	'2019-05-09 01:48:59');

DROP TABLE IF EXISTS `attendance_correction`;
CREATE TABLE `attendance_correction` (
  `attendance` int(10) unsigned NOT NULL COMMENT 'attendane.handle',
  `mask` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0x01 - Accepted by SuperVisior; 0x02 - Declined by SuperVisior; 0x04 - Accepted by HRD; 0x08 - Declined by HRD; CronDaily: Update Attendance and Delete if mask = (0x01 | 0x04); CronFrequently: Update if no Supervisior',
  `onduty` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `offduty` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `message` text NOT NULL COMMENT 'By Staff',
  `time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT 'CronDaily: Delete if (time < NOW()) && (mask IS Declined)',
  UNIQUE KEY `attendance_u` (`attendance`),
  KEY `attendance_i` (`attendance`),
  CONSTRAINT `attendance_correction_ibfk_1` FOREIGN KEY (`attendance`) REFERENCES `attendance` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `attendance_correction`;
INSERT INTO `attendance_correction` (`attendance`, `mask`, `onduty`, `offduty`, `message`, `time`) VALUES
(1,	5,	'2018-04-29 11:20:00',	'2019-04-30 11:20:00',	'asd',	'2019-05-13 03:45:12');

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `config`;
INSERT INTO `config` (`key`, `value`, `description`) VALUES
('attendance_point',	'1',	'nilai jam masuk'),
('attendance_time',	'07:15',	'batas jam masuk'),
('dayoff_limit',	'12',	'batas cuti pertahun'),
('dayoff_period',	'3',	'akumulasi cuti dalam tahun');

DROP TABLE IF EXISTS `dayoff`;
CREATE TABLE `dayoff` (
  `handle` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account` int(10) unsigned NOT NULL,
  `mask` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0x01 - Accepted by SuperVisior; 0x02 - Declined by SuperVisior; 0x04 - Accepted by HRD; 0x08 - Approved by HRD; CronFrequently: Update if no Supervisior',
  `start` date NOT NULL DEFAULT '1970-01-01',
  `finish` date NOT NULL DEFAULT '1970-01-01',
  `message` text NOT NULL,
  `time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`handle`),
  KEY `account` (`account`),
  CONSTRAINT `dayoff_ibfk_1` FOREIGN KEY (`account`) REFERENCES `account` (`handle`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `dayoff`;

DROP VIEW IF EXISTS `_aaa`;
CREATE TABLE `_aaa` (`token` varchar(40), `ip` varchar(16), `account` decimal(10,0), `privilege` decimal(10,0), `time` datetime);


DROP VIEW IF EXISTS `_account`;
CREATE TABLE `_account` (`handle` int(10) unsigned, `username` varchar(24), `password` varchar(28), `nickname` varchar(24), `privilege` bigint(21) unsigned);


DROP VIEW IF EXISTS `_attendance`;
CREATE TABLE `_attendance` (`handle` int(10) unsigned, `account` int(10) unsigned, `nickname` varchar(24), `onduty` datetime, `offduty` datetime, `time` datetime);


DROP VIEW IF EXISTS `_attendance_correction`;
CREATE TABLE `_attendance_correction` (`attendance` int(10) unsigned, `account` int(10) unsigned, `supervisior` decimal(10,0), `nickname` varchar(24), `mask` int(10) unsigned, `onduty` datetime, `offduty` datetime, `message` text);


DROP VIEW IF EXISTS `_attendance_hrd`;
CREATE TABLE `_attendance_hrd` (`attendance` int(10) unsigned, `nickname` varchar(24), `onduty` datetime, `offduty` datetime, `message` text);


DROP VIEW IF EXISTS `_attendance_supervisior`;
CREATE TABLE `_attendance_supervisior` (`attendance` int(10) unsigned, `supervisior` decimal(10,0), `nickname` varchar(24), `onduty` datetime, `offduty` datetime, `message` text);


DROP TABLE IF EXISTS `_aaa`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_aaa` AS select `aaa_token`.`value` AS `token`,`aaa_auth`.`ip` AS `ip`,ifnull(`account`.`handle`,0) AS `account`,ifnull(`account`.`privilege`,0) AS `privilege`,`aaa_token`.`time` AS `time` from ((`aaa_token` left join `aaa_auth` on((`aaa_auth`.`handle` = `aaa_token`.`auth`))) left join `account` on((`account`.`handle` = `aaa_token`.`account`))) where (`aaa_token`.`time` > now());

DROP TABLE IF EXISTS `_account`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_account` AS select `account`.`handle` AS `handle`,`account`.`username` AS `username`,`account`.`password` AS `password`,`account`.`nickname` AS `nickname`,(`account`.`privilege` | if(((select count(0) from `account_hierarchy` where (`account_hierarchy`.`supervisior` = `account`.`handle`)) > 0),8,0)) AS `privilege` from `account`;

DROP TABLE IF EXISTS `_attendance`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_attendance` AS select `attendance`.`handle` AS `handle`,`attendance`.`account` AS `account`,`account`.`nickname` AS `nickname`,`attendance`.`onduty` AS `onduty`,`attendance`.`offduty` AS `offduty`,`attendance`.`time` AS `time` from (`attendance` join `account` on((`account`.`handle` = `attendance`.`account`)));

DROP TABLE IF EXISTS `_attendance_correction`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_attendance_correction` AS select `attendance_correction`.`attendance` AS `attendance`,`attendance`.`account` AS `account`,ifnull(`account_hierarchy`.`supervisior`,0) AS `supervisior`,`account`.`nickname` AS `nickname`,`attendance_correction`.`mask` AS `mask`,`attendance_correction`.`onduty` AS `onduty`,`attendance_correction`.`offduty` AS `offduty`,`attendance_correction`.`message` AS `message` from (((`attendance_correction` join `attendance` on((`attendance`.`handle` = `attendance_correction`.`attendance`))) left join `account_hierarchy` on((`account_hierarchy`.`account` = `attendance`.`account`))) join `account` on((`account`.`handle` = `attendance`.`account`)));

DROP TABLE IF EXISTS `_attendance_hrd`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_attendance_hrd` AS select `_attendance_correction`.`attendance` AS `attendance`,`_attendance_correction`.`nickname` AS `nickname`,`_attendance_correction`.`onduty` AS `onduty`,`_attendance_correction`.`offduty` AS `offduty`,`_attendance_correction`.`message` AS `message` from `_attendance_correction` where (`_attendance_correction`.`mask` = 1);

DROP TABLE IF EXISTS `_attendance_supervisior`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `_attendance_supervisior` AS select `_attendance_correction`.`attendance` AS `attendance`,`_attendance_correction`.`supervisior` AS `supervisior`,`_attendance_correction`.`nickname` AS `nickname`,`_attendance_correction`.`onduty` AS `onduty`,`_attendance_correction`.`offduty` AS `offduty`,`_attendance_correction`.`message` AS `message` from `_attendance_correction` where (`_attendance_correction`.`mask` = 0);

-- 2019-05-12 20:45:51
