<?php
	$include = dirname(__FILE__) . DIRECTORY_SEPARATOR . "php";
	if(is_dir($include)){
		foreach(scandir($include) as $scandir){
			$realpath = realpath($include . DIRECTORY_SEPARATOR . $scandir);
			if(
				is_file($realpath) &&
				(mime_content_type($realpath) == "text/x-php") &&
				(pathinfo($realpath, PATHINFO_EXTENSION) == "php")
			){
				include($realpath);
			}
		}
	}
?>