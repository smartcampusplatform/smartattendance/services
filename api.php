<?php
	include("include.php");
	
	$hSQL = SQLOpen();
	
	
	$token = GetToken();
	$class = (isset($_GET["class"]) ? $_GET["class"] : "");
	$method = (isset($_GET["method"]) ? $_GET["method"] : "");
	$parameter = GetInput();
	
	$output = array(
		"result" => null,
		// "token" => $token,
		"class" => $class,
		"method" => $method,
		"parameter" => $parameter,
		"time" => "1970-01-01 00:00:00",
		"message" => ""
	);	
	
	$query = SQLQuery($hSQL, 
		"SELECT COUNT(0) AS `count` " .
		"FROM `_aaa` " .
		"WHERE " .
			"(`_aaa`.`token` LIKE '" . $hSQL->real_escape_string($token) . "') AND " .
			"(`_aaa`.`ip` LIKE '" . $hSQL->real_escape_string(GetClientIP()) . "')"
	);
	
	if(intval($query[0]["count"]) == 1){
		if(class_exists($class) && is_subclass_of($class, "api")){
			$object = new $class($hSQL, $token);
			if(method_exists($object, $method)){
				try{
					$output["result"] = call_user_func(array($object, $method), $parameter);					
					$output["time"] = call_user_func(array($object, "AAATime"));
					$output["message"] = call_user_func(array($object, "GetMessage"));
				}catch(Exception $exception){
					error_log("API(" . $exception->getMessage() . "): " . json_encode(array(
						"token" => $token,
						"class" => $class,
						"method" => $method,
						"parameter" => $parameter,
						"time" => date("Y-m-d H:i:s")
					)));
					
					$output["message"] = $exception->getMessage();
				}	
			}else{
				$output["message"] = "invalid method";	
			}
		}else{
			$output["message"] = "invalid class";	
		}
	}else{
		$output["message"] = "invalid token and ip";
	}
	
	SQLClose($hSQL);
	
	$json = json_encode($output);
	header("Content-type: application/json; charset=UTF-8");
	header("Content-Length: " . strlen($json));
	echo($json);
?>