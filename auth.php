<?php
	include("include.php");
	
	$hSQL = SQLOpen();
	$ip = GetClientIP();
	$input = GetInput();	
	$output = array(
		"token" => "",
		"ip" => $ip,
		"time" => "1907-01-01 00:00:00",
		"message" => ""
	);	
	
	$query = SQLQuery($hSQL, 
		"SELECT `aaa_auth`.`handle` " .
		"FROM `aaa_auth` " .
		"WHERE " .
			"(`aaa_auth`.`ip` LIKE '" . $hSQL->real_escape_string($ip) . "') AND " .
			"(`aaa_auth`.`key` LIKE '" . $hSQL->real_escape_string((isset($input["key"]) ? $input["key"] : "")) . "') " .
		"LIMIT 1"
	);
	
	if(count($query) == 1){	
		$auth = intval($query[0]["handle"]);
			
		$query = SQLQuery($hSQL, 
			"SELECT " .
				"`aaa_token`.`value` AS `token`, " .
				"`aaa_token`.`time` AS `time` " .
			"FROM `aaa_token` " .
			"WHERE " .
				"(`aaa_token`.`auth` = " . $auth . ") AND " .
				"(`aaa_token`.`value` LIKE '" . $hSQL->real_escape_string((isset($input["token"]) ? $input["token"] : "")) . "') " .
			"LIMIT 1"
		);
		
		if(count($query) == 1){	
			$output["token"] = $query[0]["token"];
			$output["time"] = $query[0]["time"];
		}else{
			$token = Generate(40);
			
			if(SQLExecute($hSQL, 
				"INSERT INTO `aaa_token`(`auth`, `value`, `time`) " .
				"VALUES(" .
					$auth . ", " .
					"'" . $hSQL->real_escape_string($token) . "', " .
					"DATE_ADD(NOW(), INTERVAL 1 DAY)" .
				")"
			)){
				$query = SQLQuery($hSQL, 
					"SELECT `aaa_token`.`time` AS `time` " .
					"FROM `aaa_token` " .
					"WHERE `aaa_token`.`value` LIKE '" . $hSQL->real_escape_string($token) . "' " .
					"LIMIT 1"
				);
				
				if(count($query) == 1){	
					$output["token"] = $token;
					$output["time"] = $query[0]["time"];
				}else{
					$output["message"] = "internal server error";				
				}
			}else{
				$output["message"] = "internal server error";				
			}
		}
	}else{
		$output["message"] = "invalid key or ip";
	}
	
	SQLClose($hSQL);
	
	$json = json_encode($output);
	header("Content-type: application/json; charset=UTF-8");
	header("Content-Length: " . strlen($json));
	echo($json);
?>