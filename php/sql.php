<?php
	function SQLOpen(){
		$ret = @new mysqli(MYSQL_HOST, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
		
		if($ret->connect_errno){
			error_log("DBOpen(" . $ret->connect_error . "): " . json_encode(array(
				"host" => MYSQL_HOST,
				"username" => MYSQL_USERNAME,
				"password" => MYSQL_PASSWORD,
				"database" => MYSQL_DATABASE,
				"port" => MYSQL_PORT
			)));
		}
		
		return $ret;
	}
	
	function SQLClose(&$hSQL){
		$hSQL->close();
	}

	function SQLExecute(&$hSQL, $Command, &$result = array()){
		$hSQL->query($Command);
		
		if($hSQL->errno){
			$ret = false;
			error_log("SQLExecute(" . $hSQL->error . "): " . $Command);
		}else{			
			$ret = true;
			$result = array(
				"affect" => $hSQL->affected_rows,
				"insert" => $hSQL->insert_id
			);
		}
		
		return $ret;
	}		

	function SQLQuery(&$hSQL, $Command){
		$ret = array();

		if($result = $hSQL->query($Command)){
			$ret = $result->fetch_all(MYSQLI_ASSOC);
			$result->close();
		}else{
			error_log("SQLQuery(" . $hSQL->error . "): " . $Command);
		}

		return $ret;
	}
?>