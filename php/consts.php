<?php
	define("MYSQL_HOST",			"db-smartattendance");
	define("MYSQL_USERNAME",		"user");
	define("MYSQL_PASSWORD",		"");
	define("MYSQL_DATABASE",		"smartattendance");
	define("MYSQL_PORT",			3306);
	
	define("QRCODE_URL",			"http://178.128.104.74/smartattendance/server/qrcode-");
	define("QRCODE_SIZE",			24);
	define("QRCODE_MARGIN",			1);
	
	define("RECORD_LIMIT",			25);
	
	define("PRIVILEGE_SYSADMIN",	0x01);
	define("PRIVILEGE_HRD",			0x02);
	define("PRIVILEGE_HRD2",		0x04);
	define("PRIVILEGE_SUPERVISIOR",	0x08);
	
	define("AC_SV_ACCEPT",			0x01); // attendance correction - supervisior
	define("AC_SV_DECLINE",			0x02); // attendance correction - supervisior
	define("AC_HRD_ACCEPT",			0x04); // attendance correction - hrd
	define("AC_HRD_DECLINE",		0x08); // attendance correction - hrd
	
	define("DO_SV_ACCEPT",			0x01); // dayoff - supervisior
	define("DO_SV_DECLINE",			0x02); // dayoff - supervisior
	define("DO_HRD_ACCEPT",			0x04); // dayoff - hrd
	define("DO_HRD_DECLINE",		0x08); // dayoff - hrd
?>
