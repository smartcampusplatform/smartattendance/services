<?php
	if(!class_exists("attendance")){
		class attendance extends API{
			private function statement($Offset, $Account){
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `_attendance` " .
					"WHERE `_attendance`.`account` = " . $Account
				);
				$limit = floor(intval($buffer[0]["count"]) / RECORD_LIMIT);
				if(($limit > 0) && (($buffer[0]["count"] % RECORD_LIMIT) == 0)){
					$limit -= 1;
				}

				if($Offset > $limit){
					$Offset = $limit;
				}

				if($Offset < 0){
					$Offset = 0;
				}
				
				return array(
					"offset" => $Offset,
					"limit" => $limit,
					"record" => array_map(
						function($data){							
							return ObjectInt($data, array("handle"));
							
						},
						SQLQuery(
							$this->hSQL, 
							"SELECT " .
								"`_attendance`.`handle` as `handle`, " .
								"`_attendance`.`nickname` as `nickname`, " .
								"`_attendance`.`onduty` as `onduty`, " .
								"`_attendance`.`offduty` as `offduty`, " .
								"`_attendance`.`time` as `time` " .
							"FROM `_attendance` " .
							"WHERE `_attendance`.`account` = " . $Account . " " .
							"LIMIT " . ($Offset * RECORD_LIMIT) . ", " . RECORD_LIMIT
						)
					)
				);
			}
			
			public function record($Data = array()){
				return $this->statement((isset($Data["offset"]) ? (is_numeric($Data["offset"]) ? $Data["offset"] : 0) : 0), $this->account());
			}
			
			public function review($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$ret = $this->statement(
						(isset($Data["offset"]) ? (is_numeric($Data["offset"]) ? $Data["offset"] : 0) : 0), 
						(isset($Data["account"]) ? (is_numeric($Data["account"]) ? $Data["account"] : 0) : 0)
					);
				}else{
					$ret = array(
						"offset" => 0,
						"limit" => 0,
						"record" => array()
					);
				}

				return $ret;
			}
			
			public function get($Data = array()){
				$ret = array();
				// if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$record = SQLQuery(
						$this->hSQL, 
						"SELECT " .
							"`attendance`.`handle` as `handle`, " .
							"`attendance`.`onduty` as `onduty`, " .
							"`attendance`.`offduty` as `offduty`, " .
							"`attendance`.`time` as `time` " .
						"FROM `attendance` " .
						"WHERE " .
							"(`attendance`.`handle` = " . (isset($Data["handle"]) ? (is_numeric($Data["handle"]) ? $Data["handle"] : 0) : 0) . ") AND " .
							"(`attendance`.`account` = " . $this->account() . ") " .
						"LIMIT 1"
					);
					
					if(count($record) > 0){
						$ret = ObjectInt($record[0], array("handle"));
					}
				// }

				return $ret;
			}
			
			public function create($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"account" => 0,
						"onduty" => "1970-01-01 00:00:00",
						"offduty" => "1970-01-01 00:00:00"
					));

					$ret = SQLExecute($this->hSQL, 
						"INSERT INTO `attendance`(`account`, `onduty`, `offduty`, `time`) " .
						"VALUES(" .
							$Data["account"] . ", " .
							"'" . $this->fSQL($Data["onduty"]) . "', " .
							"'" . $this->fSQL($Data["offduty"]) . "', " .
							"NOW()" .
						")"
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function update($Data = array()){
				$Data = ObjectDefault($Data, array(
					"handle" => 0,
					"onduty" => "1970-01-01 00:00:00",
					"offduty" => "1970-01-01 00:00:00"
				));
				
				$buffer = array();
				foreach(array("onduty", "offduty") as $key){
					if($Data[$key] != "1970-01-01 00:00:00"){
						array_push($buffer, "`" . $key . "` = '" . $this->fSQL($Data[$key]) . "'");
					}
				}

				if($this->GrantPrivilege(PRIVILEGE_HRD) && (count($buffer) > 0)){		
					array_push($buffer, "`time` = NOW()");			
					$ret = SQLExecute($this->hSQL, 
						"UPDATE `attendance` " .
						"SET " . implode(", ", $buffer) .
						"WHERE `attendance`.`handle` = " . $Data["handle"]
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function remove($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$ret = SQLExecute($this->hSQL, 
						"DELETE FROM `attendance` " .
						"WHERE `attendance`.`handle` = " . (isset($Data["handle"]) ? (is_numeric($Data["handle"]) ? $Data["handle"] : 0) : 0)
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function correction_request($Data = array()){
				$Data = ObjectDefault($Data, array(
					"attendance" => 0,
					"onduty" => "1970-01-01 00:00:00",
					"offduty" => "1970-01-01 00:00:00",
					"message" => ""
				));
								
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `attendance` " .
					"WHERE " . 
						"(`attendance`.`handle` = " . $Data["attendance"] . ") AND " .
						"(`attendance`.`account` = " . $this->account() . ")"
				);
				
				if($buffer[0]["count"] == 1){
					$ret = SQLExecute($this->hSQL, 
						"INSERT INTO `attendance_correction`(`attendance`, `onduty`, `offduty`, `message`, `time`) " .
						"VALUES(" .
							$Data["attendance"] . ", " .
							"'" . $this->fSQL($Data["onduty"]) . "', " .
							"'" . $this->fSQL($Data["offduty"]) . "', " .
							"'" . $this->fSQL($Data["message"]) . "', " .
							"NOW()" .
						")" .
						"ON DUPLICATE KEY UPDATE " .
							"`mask` = 0, " .
							"`onduty` = '" . $this->fSQL($Data["onduty"]) . "', " .
							"`offduty` = '" . $this->fSQL($Data["offduty"]) . "', " .
							"`message` = '" . $this->fSQL($Data["message"]) . "', " .
							"`time` = NOW()"
					);		
					$ret = $Data;
				}else{
					$ret = false;
				}
				
				return $ret;
			}
			
			public function supervisior($Data = array()){
				$offset = (isset($Data["offset"]) ? (is_numeric($Data["offset"]) ? $Data["offset"] : 0) : 0);
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `_attendance_supervisior` " .
					"WHERE `_attendance_supervisior`.`supervisior` = " . $this->account()
				);
				$limit = floor(intval($buffer[0]["count"]) / RECORD_LIMIT);
				if(($limit > 0) && (($buffer[0]["count"] % RECORD_LIMIT) == 0)){
					$limit -= 1;
				}

				if($offset > $limit){
					$offset = $limit;
				}

				if($offset < 0){
					$offset = 0;
				}
				
				return array(
					"offset" => $offset,
					"limit" => $limit,
					"record" => array_map(
						function($data){							
							return ObjectInt($data, array("attendance"));							
						},
						SQLQuery(
							$this->hSQL, 
							"SELECT " .
								"`_attendance_supervisior`.`attendance` as `attendance`, " .
								"`_attendance_supervisior`.`nickname` as `nickname`, " .
								"`_attendance_supervisior`.`onduty` as `onduty`, " .
								"`_attendance_supervisior`.`offduty` as `offduty`, " .
								"`_attendance_supervisior`.`message` as `message` " .
							"FROM `_attendance_supervisior` " .
							"WHERE `_attendance_supervisior`.`supervisior` = " . $this->account() . " " .
							"LIMIT " . ($offset * RECORD_LIMIT) . ", " . RECORD_LIMIT
						)
					)
				);
			}
						
			public function hrd($Data = array()){
				$offset = (isset($Data["offset"]) ? (is_numeric($Data["offset"]) ? $Data["offset"] : 0) : 0);
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `_attendance_hrd`"
				);
				$limit = floor(intval($buffer[0]["count"]) / RECORD_LIMIT);
				if(($limit > 0) && (($buffer[0]["count"] % RECORD_LIMIT) == 0)){
					$limit -= 1;
				}

				if($offset > $limit){
					$offset = $limit;
				}

				if($offset < 0){
					$offset = 0;
				}
				
				return array(
					"offset" => $offset,
					"limit" => $limit,
					"record" => array_map(
						function($data){							
							return ObjectInt($data, array("attendance"));							
						},
						SQLQuery(
							$this->hSQL, 
							"SELECT " .
								"`_attendance_hrd`.`attendance` as `attendance`, " .
								"`_attendance_hrd`.`nickname` as `nickname`, " .
								"`_attendance_hrd`.`onduty` as `onduty`, " .
								"`_attendance_hrd`.`offduty` as `offduty`, " .
								"`_attendance_hrd`.`message` as `message` " .
							"FROM `_attendance_hrd` " .
							"LIMIT " . ($offset * RECORD_LIMIT) . ", " . RECORD_LIMIT
						)
					)
				);
			}
			
			public function correction_supervisior($Data = array()){		
				$Data = ObjectDefault($Data, array(
					"handle" => 0,
					"decision" => false
				));
				
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `_attendance_correction` " .
					"WHERE " . 
						"(`_attendance_correction`.`attendance` = " . $Data["handle"] . ") AND " .
						"(`_attendance_correction`.`supervisior` = " . $this->account() . ") AND " .
						"((`_attendance_correction`.`mask` & " . (AC_SV_ACCEPT | AC_SV_DECLINE) . ") = 0)"
				);
				
				if($buffer[0]["count"] == 1){
					$ret = SQLExecute($this->hSQL, 
						"UPDATE `attendance_correction` " .
						"SET " . 
							"`mask` = (`mask` | " . ($Data["decision"] ? AC_SV_ACCEPT : AC_SV_DECLINE) . "), " .
							"`time` = NOW() " .
						"WHERE `attendance_correction`.`attendance` = " . $Data["handle"]
					);
					$this->correction_copy($Data["handle"]);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function correction_hrd($Data = array()){
				$ret = false;
				$Data = ObjectDefault($Data, array(
					"handle" => 0,
					"decision" => false
				));
				
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					SQLExecute($this->hSQL, 
						"UPDATE `attendance_correction` " .
						"SET " . 
							"`mask` = (`mask` | " . ($Data["decision"] ? AC_HRD_ACCEPT : AC_HRD_DECLINE) . "), " .
							"`time` = NOW() " .
						"WHERE " .
							"(`attendance_correction`.`attendance` = " . $Data["handle"] . ") AND " .
							"((`attendance_correction`.`mask` & " . (AC_HRD_ACCEPT | AC_HRD_DECLINE) . ") = 0)"
					);
					$this->correction_copy($Data["handle"]);
				}
				
				return $ret;
			}
			
			private function correction_copy($Handle){			
				$record = SQLQuery(
					$this->hSQL, 
					"SELECT " .
						"`attendance_correction`.`onduty` as `onduty`, " .
						"`attendance_correction`.`offduty` as `offduty` " .
					"FROM `attendance_correction` " .
					"WHERE " .
						"(`attendance_correction`.`attendance` = " . $Handle . ") AND " .
						"(`attendance_correction`.`mask` = " . (AC_SV_ACCEPT | AC_HRD_ACCEPT) . ") " .
					"LIMIT 1"
				);

				if(count($record) > 0){						
					$buffer = array();
					foreach(array("onduty", "offduty") as $key){
						if($record[0][$key] != "1970-01-01 00:00:00"){
							array_push($buffer, "`" . $key . "` = '" . $this->fSQL($record[0][$key]) . "'");
						}
					}

					if(count($buffer) > 0){		
						array_push($buffer, "`time` = NOW()");			
						$ret = SQLExecute($this->hSQL, 
							"UPDATE `attendance` " .
							"SET " . implode(", ", $buffer) .
							"WHERE `attendance`.`handle` = " . $Handle
						);
					}
				}
			}
		}
	}
?>