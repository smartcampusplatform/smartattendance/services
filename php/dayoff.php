<?php
	if(!class_exists("dayoff")){
		class dayoff extends API{
			private function statement($Offset, $Account){
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `_attendance` " .
					"WHERE `_attendance`.`account` = " . $Account
				);
				$limit = floor(intval($buffer[0]["count"]) / RECORD_LIMIT);
				if(($limit > 0) && (($buffer[0]["count"] % RECORD_LIMIT) == 0)){
					$limit -= 1;
				}

				if($Offset > $limit){
					$Offset = $limit;
				}

				if($Offset < 0){
					$Offset = 0;
				}
				
				return array(
					"offset" => $Offset,
					"limit" => $limit,
					"record" => array_map(
						function($data){							
							return ObjectInt($data, array("handle"));
							
						},
						SQLQuery(
							$this->hSQL, 
							"SELECT " .
								"`_attendance`.`handle` as `handle`, " .
								"`_attendance`.`nickname` as `nickname`, " .
								"`_attendance`.`onduty` as `onduty`, " .
								"`_attendance`.`offduty` as `offduty` " .
							"FROM `_attendance` " .
							"WHERE `_attendance`.`account` = " . $Account . " " .
							"LIMIT " . ((is_numeric($Offset) ? $Offset : 0) * RECORD_LIMIT) . ", " . RECORD_LIMIT
						)
					)
				);
			}
			
			public function record($Offset = 0){
				return $this->statement($Offset, $this->account());
			}
			
			public function review($Offset = 0, $Account = 0){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$ret = $this->statement($Offset, $Account);
				}else{
					$ret = array(
						"offset" => 0,
						"limit" => 0,
						"record" => array()
					);
				}

				return $ret;
			}
			
			public function get($Handle = 0){
				$ret = array();
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$record = SQLQuery(
						$this->hSQL, 
						"SELECT " .
							"`attendance`.`handle` as `handle`, " .
							"`attendance`.`onduty` as `onduty`, " .
							"`attendance`.`offduty` as `offduty` " .
						"FROM `attendance` " .
						"WHERE `attendance`.`handle` = " . (is_numeric($Handle) ? $Handle : 0) . " " .
						"LIMIT 1"
					);
					
					if(count($record) > 0){
						$ret = ObjectInt($record[0], array("handle"));
					}
				}

				return $ret;
			}
			
			public function create($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"account" => 0,
						"onduty" => "1970-01-01 00:00:00",
						"offduty" => "1970-01-01 00:00:00"
					));

					$ret = SQLExecute($this->hSQL, 
						"INSERT INTO `attendance`(`account`, `onduty`, `offduty`, `time`) " .
						"VALUES(" .
							$Data["account"] . ", " .
							"'" . $this->fSQL($Data["onduty"]) . "', " .
							"'" . $this->fSQL($Data["offduty"]) . "', " .
							"NOW()" .
						")"
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function update($Data = array()){
				$Data = ObjectDefault($Data, array(
					"handle" => 0,
					"onduty" => "1970-01-01 00:00:00",
					"offduty" => "1970-01-01 00:00:00"
				));
				
				$buffer = array();
				foreach(array("onduty", "offduty") as $key){
					if($Data[$key] != "1970-01-01 00:00:00"){
						array_push($buffer, "`" . $key . "` = '" . $this->fSQL($Data[$key]) . "'");
					}
				}

				if($this->GrantPrivilege(PRIVILEGE_HRD) && (count($buffer) > 0)){		
					array_push($buffer, "`time` = NOW()");			
					$ret = SQLExecute($this->hSQL, 
						"UPDATE `attendance` " .
						"SET " . implode(", ", $buffer) .
						"WHERE `attendance`.`handle` = " . $Data["handle"]
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function remove($Handle = 0){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$ret = SQLExecute($this->hSQL, 
						"DELETE FROM `attendance` " .
						"WHERE `attendance`.`handle` = " . (is_numeric($Handle) ? $Handle : 0)
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function correction_request($Data = array()){
				$Data = ObjectDefault($Data, array(
					"attendance" => 0,
					"onduty" => "1970-01-01 00:00:00",
					"offduty" => "1970-01-01 00:00:00",
					"message" => ""
				));
								
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `attendance` " .
					"WHERE " . 
						"(`attendance`.`handle` = " . $Data["attendance"] . ") AND " .
						"(`attendance`.`account` = " . $this->account() . ")"
				);
				
				if($buffer[0]["count"] == 1){
					$ret = SQLExecute($this->hSQL, 
						"INSERT INTO `attendance_correction`(`attendance`, `onduty`, `offduty`, `message`, `time`) " .
						"VALUES(" .
							$Data["attendance"] . ", " .
							"'" . $this->fSQL($Data["onduty"]) . "', " .
							"'" . $this->fSQL($Data["offduty"]) . "', " .
							"'" . $this->fSQL($Data["message"]) . "', " .
							"NOW()" .
						")" .
						"ON DUPLICATE KEY UPDATE " .
							"`mask` = 0, " .
							"`onduty` = '" . $this->fSQL($Data["onduty"]) . "', " .
							"`offduty` = '" . $this->fSQL($Data["offduty"]) . "', " .
							"`message` = '" . $this->fSQL($Data["message"]) . "', " .
							"`time` = NOW()"
					);		
				}else{
					$ret = false;
				}
				
				return $ret;
			}
			
			public function correction_supervisior($Handle = 0, $Decision = false){							
				$buffer = SQLQuery($this->hSQL, 
					"SELECT COUNT(0) as `count` " .
					"FROM `_attendance_correction` " .
					"WHERE " . 
						"(`_attendance_correction`.`attendance` = " . $Handle . ") AND " .
						"(`_attendance_correction`.`supervisior` = " . $this->account() . ") AND " .
						"((`_attendance_correction`.`mask` & " . (AC_SV_ACCEPT | AC_SV_DECLINE) . ") = 0)"
				);
				
				if($buffer[0]["count"] == 1){
					$ret = SQLExecute($this->hSQL, 
						"UPDATE `attendance_correction` " .
						"SET " . 
							"`mask` = (`mask` | " . ($Decision ? AC_SV_ACCEPT : AC_SV_DECLINE) . "), " .
							"`time` = NOW() " .
						"WHERE `attendance_correction`.`attendance` = " . $Handle
					);		
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function correction_hrd($Handle = 0, $Decision = false){
				return (
					$this->GrantPrivilege(PRIVILEGE_HRD) ?
					SQLExecute($this->hSQL, 
						"UPDATE `attendance_correction` " .
						"SET " . 
							"`mask` = (`mask` | " . ($Decision ? AC_HRD_ACCEPT : AC_HRD_DECLINE) . "), " .
							"`time` = NOW() " .
						"WHERE " .
							"(`attendance_correction`.`attendance` = " . $Handle . ") AND " .
							"((`attendance_correction`.`mask` & " . (AC_HRD_ACCEPT | AC_HRD_DECLINE) . ") = 0)"
					) :
					false
				);
			}
			
			public function offduty($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"handle" => 0,
						"time" => "1970-01-01 00:00:00"
					));

					$ret = SQLExecute($this->hSQL, 
						"UPDATE `attendance` " .
						"SET `offduty` = '" . $this->fSQL($Data["time"]) . "' " .
						"WHERE `attendance`.`handle` = " . $Data["handle"]
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
		}
	}
?>