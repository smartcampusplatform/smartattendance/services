<?php
	if(!class_exists("sign")){
		class sign extends API{
			public function in($Data){
				$Data = ObjectDefault($Data, array(
					"username" => "",
					"password" => ""
				));
				
				$ret = array(
					"nickname" => "",
					"privilege" => 0
				);
				
				$query = SQLQuery($this->hSQL, 
					"SELECT " .
						"`_account`.`handle`, " .
						"`_account`.`nickname`, " .
						"`_account`.`privilege` " .
					"FROM `_account` " .
					"WHERE " .
						"(`_account`.`username` LIKE '" . $this->fSQL($Data["username"]) . "') AND " .
						"(`_account`.`password` LIKE '" . base64_encode(sha1($Data["password"], true)) . "') " .
					"LIMIT 1"
				);
				
				if(count($query) == 1){
					SQLExecute($this->hSQL, 
						"UPDATE `aaa_token` " .
						"SET `account` = null " .
						"WHERE `aaa_token`.`account` = " . $query[0]["handle"]
					);				
					
					if(SQLExecute($this->hSQL, 
						"UPDATE `aaa_token` " .
						"SET `account` = " . $query[0]["handle"] . " " .
						"WHERE `aaa_token`.`value` LIKE '" . $this->fSQL($this->token) . "'"
					)){
						$ret["nickname"] = $query[0]["nickname"];
						$ret["privilege"] = intval($query[0]["privilege"]);
					}					
				}
				
				return $ret;
			}
			
			public function info(){
				$ret = array(
					"nickname" => "",
					"privilege" => 0
				);
				
				$query = SQLQuery($this->hSQL, 
					"SELECT " .
						"`_account`.`nickname`, " .
						"`_account`.`privilege` " .
					"FROM `_account` " .
					"WHERE `_account`.`handle` = " . $this->account() . " " .
					"LIMIT 1"
				);
				
				if(count($query) == 1){
					$ret["nickname"] = $query[0]["nickname"];
					$ret["privilege"] = intval($query[0]["privilege"]);			
				}
				
				return $ret;
			}
			
			public function out(){
				return SQLExecute($this->hSQL, 
					"UPDATE `aaa_token` " .
					"SET `account` = null " .
					"WHERE `aaa_token`.`value` LIKE '" . $this->fSQL($this->token) . "'"
				);
			}
		}
	}
?>