<?php
	if(!class_exists("account")){
		class account extends API{
			public function record($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){					
					$offset = (isset($Data["offset"]) ? (is_numeric($Data["offset"]) ? $Data["offset"] : 0) : 0);
					
					$buffer = SQLQuery($this->hSQL, 
						"SELECT COUNT(0) as `count` " .
						"FROM `account` " .
						"WHERE `account`.`handle` != " . $this->account()
					);
					$limit = floor(intval($buffer[0]["count"]) / RECORD_LIMIT);
					if(($limit > 0) && (($buffer[0]["count"] % RECORD_LIMIT) == 0)){
						$limit -= 1;
					}

					if($offset > $limit){
						$offset = $limit;
					}

					if($offset < 0){
						$offset = 0;
					}

					$record = array_map(
						function($data){
							return ObjectInt($data, array("handle", "privilege"));
						},
						SQLQuery(
							$this->hSQL, 
							"SELECT " .
								"`account`.`handle` as `handle`, " .
								"`account`.`username` as `username`, " .
								"`account`.`nickname` as `nickname`, " .
								"`account`.`privilege` as `privilege` " .
							"FROM `account` " .
							"WHERE `account`.`handle` != " . $this->account() . " " .
							"LIMIT " . ($offset * RECORD_LIMIT) . ", " . RECORD_LIMIT
						)
					);
				}else{
					$offset = 0;
					$limit = 0;
					$record = array();
				}

				return array(
					"offset" => $offset,
					"limit" => $limit,
					"record" => $record
				);
			}
			
			public function get($Data = array()){
				$ret = array();
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$record = SQLQuery(
						$this->hSQL, 
						"SELECT " .
							"`account`.`handle` as `handle`, " .
							"`account`.`username` as `username`, " .
							"`account`.`nickname` as `nickname`, " .
							"`account`.`privilege` as `privilege` " .
						"FROM `account` " .
						"WHERE `account`.`handle` = " . (isset($Data["handle"]) ? (is_numeric($Data["handle"]) ? $Data["handle"] : 0) : 0) . " " .
						"LIMIT 1"
					);
					
					if(count($record) > 0){
						$ret = ObjectInt($record[0], array("handle", "privilege"));
					}
				}

				return $ret;
			}
			
			public function create($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"username" => "",
						"nickname" => "",
						"password" => "",
						"privilege" => 0
					));

					$Data["password"] = base64_encode(sha1($Data["password"], true));
					$ret = SQLExecute($this->hSQL, 
						"INSERT INTO `account`(`username`, `nickname`, `password`, `privilege`) " .
						"VALUES(" .
							"'" . $this->fSQL($Data["username"]) . "', " .
							"'" . $this->fSQL($Data["nickname"]) . "', " .
							"'" . $this->fSQL($Data["password"]) . "', " .
							$Data["privilege"] . 
						")"
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function update($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"handle" => 0,
						"username" => "",
						"nickname" => "",
						"privilege" => 0
					));

					$ret = SQLExecute($this->hSQL, 
						"UPDATE `account` " .
						"SET " .
							"`username` = '" . $this->fSQL($Data["username"]) . "', " .
							"`nickname` = '" . $this->fSQL($Data["nickname"]) . "', " .
							"`privilege` = " . $Data["privilege"] . " " .
						"WHERE `account`.`handle` = " . $Data["handle"]
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function remove($Data = array()){
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$ret = SQLExecute($this->hSQL, 
						"DELETE FROM `account` " .
						"WHERE `account`.`handle` = " . (isset($Data["handle"]) ? (is_numeric($Data["handle"]) ? $Data["handle"] : 0) : 0)
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function recovery($Data = array()){ // reset password
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"handle" => 0,
						"password" => ""
					));

					$ret = SQLExecute($this->hSQL, 
						"UPDATE `account` " .
						"SET `password` = '" . $this->fSQL(base64_encode(sha1($Data["password"], true))) . "' " .
						"WHERE `account`.`handle` = " . $Data["handle"]
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function password($Data = array()){ // change my password
				if($this->GrantPrivilege(PRIVILEGE_HRD)){
					$Data = ObjectDefault($Data, array(
						"old" => "",
						"new" => ""
					));

					$ret = SQLExecute($this->hSQL, 
						"UPDATE `account` " .
						"SET `password` = '" . $this->fSQL(base64_encode(sha1($Data["new"], true))) . "' " .
						"WHERE " . 
							"(`account`.`handle` = " . $this->account() . ") AND " .
							"(`account`.`password` LIKE '" . $this->fSQL(base64_encode(sha1($Data["old"], true))) . "')"
					);
				}else{
					$ret = false;
				}

				return $ret;
			}
			
			public function option(){
				return array_map(
					function($data){
						return ObjectInt($data, array("handle", "privilege"));
					},
					SQLQuery(
						$this->hSQL, 
						"SELECT " .
							"`account`.`handle` as `handle`, " .
							"`account`.`nickname` as `nickname`, " .
							"`account`.`privilege` as `privilege` " .
						"FROM `account`"
					)
				);
			}
		}
	}
?>