<?php
	function GetClientIP(){
		$ret = "";
		
		foreach(array(
			"HTTP_CLIENT_IP",
			"HTTP_X_FORWARDED_FOR",
			"REMOTE_ADDR"
		) as $key){
			if(($ret == "") && isset($_SERVER[$key])){
				$ret = $_SERVER[$key];
			}
		}

		return $ret;
	}
	
	function GetToken(){
		$ret = "";
		
		foreach(apache_request_headers() as $key => $value){
			if(strtolower($key) == "token"){
				$ret = $value;
			}
		}

		return $ret;
	}
	
	function GetInput(){
		$ret = array();
		$content = file_get_contents("php://input");
		
		if($content != ""){
			$json = json_decode($content, true);
			if($json){
				$ret = $json;
			}else{
				error_log("GetInput(" . json_last_error_msg() . "): " . $content);
			}
		}

		return (count($ret) > 0 ? $ret : new stdClass());
	}

	function Generate($Length = 8){
		$ret = "";

		for($pointer = 0; $pointer < $Length; $pointer++){
			$rnd = rand(0, 35);
			$ret .= chr($rnd < 10 ? $rnd + 48 : $rnd + 55);
		}

		return $ret;
	}

	function ObjectDefault($hObject = array(), $Default = array()){
		$ret = array();
		
		if(is_array($hObject)){
			foreach($Default as $key => $value){
				if(isset($hObject[$key])){
					$wparam = gettype($hObject[$key]);
					if(($wparam == "integer") || ($wparam == "double")){
						$wparam = "number";
					}
					
					$lparam = gettype($value);
					if(($lparam == "integer") || ($lparam == "double")){
						$lparam = "number";
					}
					
					$ret[$key] = ($wparam == $lparam ? $hObject[$key] : $value);
				}else{
					$ret[$key] = $value;
				}
			}
		}else{
			$ret = $Default;
		}
		
		return $ret;
	}
	
	function ObjectInt($hObject = array(), $Enumeration = array()){
		$ret = array();
		
		foreach($hObject as $key => $value){
			$ret[$key] = (in_array($key, $Enumeration) ? intval($hObject[$key]) : $hObject[$key]);
		}
		
		return $ret;
	}
	
	function StringPattern($hAlloc, $Pattern = array()){
		$ret = $hAlloc;

		foreach($Pattern as $key => $value){
			$ret = str_ireplace("{%" . $key . "%}", $value, $ret);
		}

		return preg_replace("/\{%(.*)%\}/", "", $ret);
	}
?>