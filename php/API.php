<?php
	if(!class_exists("API")){
		class API{
			var $hSQL = 0;
			var $token = "";
			
			private $_account = 0;
			private $_privilege = 0;
			private $_message = "";

			function __construct(&$hSQL, $Token){
				$this->hSQL = $hSQL;
				$this->token = $Token;
			}
			
			public function fSQL($hAlloc){
				return $this->hSQL->real_escape_string($hAlloc);
			}
			
			public function account(){
				if($this->_account == 0){
					$query = SQLQuery($this->hSQL, 
						"SELECT `_aaa`.`account` " .
						"FROM `_aaa` " .
						"WHERE `_aaa`.`token` LIKE '" . $this->fSQL($this->token) . "' " .
						"LIMIT 1"
					);
					
					$this->_account = (count($query) == 1 ? intval($query[0]["account"]) : 0);
				}
				
				return $this->_account;
			}
			
			public function privilege(){
				if($this->_privilege == 0){
					$query = SQLQuery($this->hSQL, 
						"SELECT `_aaa`.`privilege` " .
						"FROM `_aaa` " .
						"WHERE `_aaa`.`token` LIKE '" . $this->fSQL($this->token) . "' " .
						"LIMIT 1"
					);
					
					$this->_privilege = (count($query) == 1 ? intval($query[0]["privilege"]) : 0);
				}
				
				return $this->_privilege;
			}
			
			public function GrantPrivilege($Privilege){
				if(($this->privilege() & $Privilege) == $Privilege){
					$ret = true;
				}else{
					$ret = false;
					$this->_message = "operation not permitted";
				}
				
				return $ret;
			}
			
			public function SetMessage($hAlloc){
				$this->_message = $hAlloc;
			}
			
			public function GetMessage(){
				return $this->_message;
			}
			
			public function AAATime(){
				$token = $this->fSQL($this->token);
				
				SQLExecute($this->hSQL, 
					"UPDATE `aaa_token` " .
					"SET `time` = DATE_ADD(NOW(), INTERVAL 1 DAY) " .
					"WHERE `aaa_token`.`value` LIKE '" . $token . "'"
				);
				
				$query = SQLQuery($this->hSQL, 
					"SELECT `aaa_token`.`time` " .
					"FROM `aaa_token` " .
					"WHERE `aaa_token`.`value` LIKE '" . $token . "' " .
					"LIMIT 1"
				);
				
				return (count($query) == 1 ? $query[0]["time"] : "1970-01-01 00:00:00");
			}
		}
	}
?>